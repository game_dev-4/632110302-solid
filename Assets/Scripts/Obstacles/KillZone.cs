using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent<HealthPoint>(out var _HealthPoint))
        {
            _HealthPoint.DecreaseHp(9999);
        }

        if (col.TryGetComponent<IDamageable>(out var _damageable))
        {
            _damageable.ApplyDamage(gameObject,9999);
        }
    }
}
