using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(AttackController))]
[RequireComponent(typeof(HealthPoint))]
public class Player : MonoBehaviour
{
    /*
    [Header("Movement")]
    [SerializeField] private Transform body;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float jumpForce = 7f;
    [SerializeField] private LayerMask groundLayer;
    private bool isGrounded;
    private Vector2 moveInput;
    */
    /*
    [Header("Health")]
    private const int MaxHp = 5;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private int currentHp = MaxHp;
    [SerializeField] private Transform spawnPoint;
    public UnityEvent<int> onHpChanged;
   */
    /*
    [Header("Attack")]
    public static event Action<WeaponData> OnChangedWeapon;
    
    [SerializeField] private WeaponData currentWeapon;
    [SerializeField] private SpriteRenderer weaponSprite;
    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject meleeHitBox;
    private bool isAttacking;
    */
    
    private Vector2 moveInput;

    //**AttackController
    private AttackController _attackControl;
    
    private void Start()
    {
        //**AttackController
        _attackControl = GetComponent<AttackController>();
    }
    /*
    #region Attack

    public void ChangeWeapon(WeaponData _newWeapon)
    {
        currentWeapon = _newWeapon;
        OnChangedWeapon?.Invoke(currentWeapon);
    }
    
    public void PerformAttack(InputAction.CallbackContext _context)
    {
        if (!_context.performed || !currentWeapon) return;

        switch (currentWeapon.AttackType)
        {
            case AttackType.MeleeAttack:
                MeleeAttack();
                break;
            case AttackType.RangeAttack:
                RangeAttack();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    

    private void MeleeAttack()
    {
        if (isAttacking) return;
        
        weaponSprite.color = currentWeapon.weaponColor;
        StartCoroutine(IEAttack());
    }

    private void RangeAttack()
    {
        var _bullet = Bullet.GetBullet(gameObject);
        _bullet.transform.position = firePoint.position;
        _bullet.Sprite.color = currentWeapon.weaponColor;
        _bullet.Fire(firePoint.right);
    }

    private IEnumerator IEAttack()
    {
        isAttacking = true;
        meleeHitBox.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        meleeHitBox.SetActive(false);
        isAttacking = false;
    }

    #endregion
    */
    /*
    #region HP

    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }
    
    public void DecreaseHp(int _value)
    {
        currentHp -= _value;
        
        if (currentHp <= 0)
        {
            Death();
        }
        onHpChanged?.Invoke(currentHp);
    }

    private void Death()
    {
        currentHp = MaxHp;
        Respawn();
    }

    private void Respawn()
    {
        rb.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }
    
    #endregion
    */
    /*
    #region Movement

    public void OnMove(InputAction.CallbackContext _context)
    {
        moveInput = _context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext _context)
    {
        if (!isGrounded) return;
        
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        isGrounded = false;
    }
    
    private readonly float checkGroundRayLenght = 0.6f;
    
    
    
    private void UpdateMovement()
    {
        rb.velocity = new Vector2(moveInput.x * moveSpeed, rb.velocity.y);
    }

    private void CheckGround()
    {
        RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, checkGroundRayLenght, groundLayer);
        
        isGrounded = _hit.collider != null;
    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, Vector3.down * checkGroundRayLenght, Color.green);
    }

    #endregion
    */
    private void FixedUpdate()
    {
        // Flip the player sprite when changing direction
        if (moveInput.x != 0) 
        {
            //**AttackController
            if(_attackControl != null)
                _attackControl.FlipDirection(moveInput.x);
        }
    }
    
}
