using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour, IDamageable
{
    [SerializeField] protected int currentHp;

    public virtual void ApplyDamage(GameObject _source, int _damage)
    {
        Debug.Log("DamageToEnemy =" + _damage);
        
        currentHp -= _damage;
        if (currentHp <= 0)
        {
            Death(_source);
        }
    }

    protected virtual void Death(GameObject _source)
    {
        Destroy(gameObject);
    }
    
}
