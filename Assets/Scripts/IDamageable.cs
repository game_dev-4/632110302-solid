using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    public void ApplyDamage(GameObject _source, int _damage);
    private void DestroyObject(GameObject _source) { }
}
