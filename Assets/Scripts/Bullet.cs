﻿using System;
using UnityEngine;


public class Bullet : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private int damage = 1;
    [SerializeField] private SpriteRenderer sprite;
    
    public GameObject Host { get; set; }

    public SpriteRenderer Sprite => sprite;

    public void Fire(Vector2 _direction, float _speed, int _damage)
    {
        rb.velocity = _direction * _speed;
        damage = _damage;
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.gameObject == Host) return;
        
        if (_col.gameObject.TryGetComponent<HealthPoint>(out var _HealthPoint))
        {
            _HealthPoint.DecreaseHp(damage);
        }

        if (_col.gameObject.TryGetComponent<IDamageable>(out var _damageable))
        {
            _damageable.ApplyDamage(Host ,damage);
        }
        Destroy(gameObject);
    }
    
    private static Bullet bulletPrefab;
    private const string path = "Bullet";
    public static Bullet GetBullet(GameObject _host)
    {
        if (!bulletPrefab)
            bulletPrefab = Resources.Load<Bullet>(path);

        var _bullet = Instantiate(bulletPrefab);
        _bullet.Host = _host;
        
        return _bullet;
    }
}
