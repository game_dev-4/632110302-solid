using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform body;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float jumpForce = 7f;
    [SerializeField] private LayerMask groundLayer;
    
    private Vector2 moveInput;
    private bool isGrounded;
    
    public void OnMove(InputAction.CallbackContext _context)
    {
        moveInput = _context.ReadValue<Vector2>();
    }
    public void OnJump(InputAction.CallbackContext _context)
    {
        if (!isGrounded) return;
        
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        isGrounded = false;
    }
    private readonly float checkGroundRayLenght = 0.6f;
    
    private void FixedUpdate()
    {
        UpdateMovement();
        CheckGround();
        if (moveInput.x != 0) 
        {
            body.localScale = new Vector3(Mathf.Sign(moveInput.x), 1f, 1f);
            AttackController _thisAttackSide = GetComponent<AttackController>();
            _thisAttackSide.FlipDirection(moveInput.x);
        }
    }
    private void UpdateMovement()
    {
        rb.velocity = new Vector2(moveInput.x * moveSpeed, rb.velocity.y);
    }
    private void CheckGround()
    {
        RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, checkGroundRayLenght, groundLayer);
        isGrounded = _hit.collider != null;
    }
    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, Vector3.down * checkGroundRayLenght, Color.green);
    }
    private void FlipCharactor()
    {
        body.localScale = new Vector3(Mathf.Sign(moveInput.x), 1f, 1f);
    }
    
}
