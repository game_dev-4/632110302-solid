using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class HealthPoint : MonoBehaviour, IDamageable
{
    [Header("Health")]
    public const int MaxHp = 5;
    [SerializeField] private int currentHp = MaxHp;
    public TextMeshProUGUI playerHP_Text;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform spawnPoint;

    public UnityEvent<int> onHpChanged;

    private void Start()
    {
        onHpChanged?.Invoke(currentHp);
    }
    private void Respawn()
    {
        rb.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }
    public void ApplyDamage(GameObject _source, int _damage)
    {
        DecreaseHp(_damage);
    }
    protected virtual void DestroyObject(GameObject _source)
    {
        Destroy(_source);
    }
    public void HealthText()
    {
        if (playerHP_Text)
            playerHP_Text.SetText("HP: "+ currentHp);
        
    }
    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }
    public void DecreaseHp(int _value) 
    {
        currentHp -= _value;
        if (currentHp <= 0)
            Death();
            
        onHpChanged?.Invoke(currentHp);
    }
    private void Death()
    {
        currentHp = MaxHp;
        Respawn();
    }
}
