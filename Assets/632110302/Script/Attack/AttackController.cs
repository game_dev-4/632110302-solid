using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    public static event Action<WeaponData> OnChangedWeapon;
    
    [Header("Attack")]
    public WeaponData currentWeapon;
    [SerializeField] private SpriteRenderer weaponSprite;
    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject meleeHitBox;
    private bool isAttacking;

    public void ChangeWeapon(WeaponData _newWeapon)
    {
        currentWeapon = _newWeapon;
        OnChangedWeapon?.Invoke(currentWeapon);
    }
    public void PerformAttack()
    {
        if (!currentWeapon) return;

        switch (currentWeapon.AttackType)
        {
            case AttackType.MeleeAttack:
                MeleeAttack();
                break;
            case AttackType.RangeAttack:
                RangeAttack();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    private void MeleeAttack()
    {
        if (isAttacking)
        {
            return;
        }
        
        weaponSprite.color = currentWeapon.weaponColor;
        StartCoroutine(IEAttack());
    }
    private void RangeAttack()
    {
        var _bullet = Bullet.GetBullet(gameObject);
        _bullet.transform.position = firePoint.position;
        _bullet.Sprite.color = currentWeapon.weaponColor;
        _bullet.Fire(firePoint.right, 10f, currentWeapon.Damage);
    }
    private IEnumerator IEAttack()
    {
        isAttacking = true;
        meleeHitBox.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        meleeHitBox.SetActive(false);
        isAttacking = false;
    }
    //// Flip the player sprite when changing direction
    public void FlipDirection(float moveInputX)
    {
        float _rotate = Mathf.Sign(moveInputX) > 0 ? 0 : 180f;
        firePoint.rotation = Quaternion.Euler(0, 0, _rotate);
    }
    //// Pick Up Weapon Item
    public void OnPlayerCollected(WeaponData _weaponData)
    {
        ChangeWeapon(_weaponData);
    }
}
