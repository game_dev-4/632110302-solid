using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AttackHitBox : MonoBehaviour
{
    public AttackController _AttackControllerWeapon;
    private void OnTriggerEnter2D(Collider2D other)
    {
        //check if player
        if(other.GetComponent<Player>())
            return;

        var damage = other.GetComponent<IDamageable>();
        
        if (damage != null)
        {
            if (_AttackControllerWeapon == null)
                damage.ApplyDamage(other.gameObject , 0);
            else
                damage.ApplyDamage(other.gameObject , _AttackControllerWeapon.currentWeapon.Damage);
        }
    }
}
